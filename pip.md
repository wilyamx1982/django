# Package Manager

1. Install the latest version of a package

		pip install AwesomePackage 
2. Install a specific version of a package

		pip install pip==19.3
		pip install AwesomePackage==2.0
		
3. Install any version of a package within a range

		pip install "AwesomePackage>=1,<2"

4. Install a "compatible" version of a package

		pip install AwesomePackage~=2.1.2 
5. To upgrade an already installed package

		pip install --upgrade PackageName 
6. Install packages using a requirements file (requirements.txt)

		AwesomePackage==2.0
		SomeOtherPackage==1.0
		XYZPackage==3.2.5 
7. It provides a list of your projects dependencies

		pip freeze 
8. To look for specific packages, use grep

		pip freeze | grep "AwesomePackage"
