# Database Management

## Contents

* Removing existing databases in preparation for dump data
* Connecting to a DB instance running the MySQL Database Engine
* Exporting and Download Data from a MySQL DB Instance by Using Replication
* Importing Data from a dump file in local environment
* Import the existing datasource file to local database
* Export data needed for user model from remote server
* Download exported data from remote server
* Import the downloaded datasource file to local database
* Switching databases between development and staging

### MySQL Commands

```
mysql> SELECT COUNT(*) FROM PoleLuxe_user;
+----------+
| COUNT(*) |
+----------+
|        0 |
+----------+
1 row in set (0.00 sec)
```

### Removing existing databases in preparation for dump data

```
mysql> show databases;
+------------------------------+
| Database                     |
+------------------------------+
| db_dev_analytics_v1_test     |
| db_dev_app_poleluxe          |
| db_dev_poleluxe              |
| db_stg_analytics_v1_test     |
| db_stg_app_poleluxe          |
| db_stg_poleluxe              |
+------------------------------+
8 rows in set (0.01 sec)

mysql> drop database db_dev_analytics_v1_test;
Query OK, 4 rows affected (0.05 sec)

mysql> drop database db_dev_app_poleluxe;
Query OK, 15 rows affected (0.09 sec)

mysql> drop database db_dev_poleluxe;
Query OK, 105 rows affected (0.68 sec)

mysql> drop database test_analytics_db;
Query OK, 4 rows affected (0.04 sec)

mysql> drop database test_db_dev_poleluxe_v1_test;
Query OK, 99 rows affected (0.70 sec)
```

### Connecting and Explore to a Remote DB instance running the MySQL Database Engine

[Connect to remote server using api working directory like **dtpapi** or **titapi**.](https://bitbucket.org/thinkittwice/documentation/src/87718350f6173af2a704077e5150d0c5155f2bdf/servers-connect.md)

Get the mysql password from **.env file** `DATABASE_URL` using command line `$ vim .env`

**DATABASES:**

* `DATABASE_URL`: workers / app-server database
* `DATABASE_URL_CMS`: app-user / api-server database
* `DATABASE_URL_ANALYTICS`: analytics database

```
DATABASE_URL=mysql://poleluxeprod:Passw0rd@titstg-defaultdb.c57umcf6o82t.us-west-1.rds.amazonaws.com:3306/dtpcmsdb
```

```
(dtpapi) ubuntu@ip-10-0-1-167:~/app$ mysql -h titstg-defaultdb.c57umcf6o82t.us-west-1.rds.amazonaws.com -P 3306 -u poleluxeprod -p
Enter password:
Welcome to the MySQL monitor...
...
mysql> show databases;
+----------------------+
| Database             |
+----------------------+
| information_schema   |
| db_app_prod_poleluxe |
| db_prod_poleluxe     |
| dtpanalyticsdb       |
| dtpcmsdb             |
| dtpworkerdb          |
| innodb               |
| mysql                |
| performance_schema   |
| sys                  |
| titanalyticsdb       |
| tmp                  |
+----------------------+
12 rows in set (0.01 sec)

mysql> quit
Bye
(dtpapi) ubuntu@ip-10-0-1-167:~/dtpapi/current$
```

### Exporting and Download Data from a MySQL DB Instance by Using Replication

**Exporting cms database** (refer database from above list)

* `dtpcmsdb` - cms/dashboard from development server
* `dtpworkerdb` - workers from development server
* `db_prod_poleluxe` - cms/dashboard users from **staging server**
* `db_app_prod_poleluxe` - workers from **staging server**

**REMOTE SERVER**

```
(api) ubuntu@ip-10-0-1-167:~/app$ mysqldump -d -h titstg-defaultdb.c57umcf6o82t.us-west-1.rds.amazonaws.com \
    -u poleluxeprod \
    -p \
    --port=3306 \
    --single-transaction \
    --routines \
    --triggers \
    --databases db_poleluxe_01 > db_poleluxe_2020-01-09.sql;
Enter password:
(api) ubuntu@ip-10-0-1-217:~/app$ ll
total 98704
drwxrwxr-x 17 ubuntu ubuntu      4096 Sep  7 01:28 ./
drwxrwxr-x  3 ubuntu ubuntu      4096 Sep  4 01:14 ../
-rwxrwxr-x  1 ubuntu ubuntu      6148 Sep  4 00:51 .DS_Store*
-rwxrwxr-x  1 ubuntu ubuntu        31 Sep  4 00:51 .bowerrc*
...
-rw-rw-r--  1 ubuntu ubuntu 100869482 Sep  7 01:29 db_prod_poleluxe.sql
...
(dtpapi) ubuntu@ip-10-0-1-217:~/titapi/current$ logout
Connection to ec2-api-stg.dailytrainingpartner.com closed.
```

```
(worker) ubuntu@ip-10-0-1-167:~/app$ mysqldump -d -h titstg-defaultdb.c57umcf6o82t.us-west-1.rds.amazonaws.com \
    -u poleluxeprod \
    -p \
    --port=3306 \
    --single-transaction \
    --routines \
    --triggers \
    --databases db_app_prod_poleluxe > db_app_prod_poleluxe_yyyy-mm-dd.sql;
Enter password:
```

**LOCAL SERVER**

```
(app-server) $ mysqldump -d \
    -u root \
    -p \
    --port=3306 \
    --single-transaction \
    --routines \
    --triggers \
    --databases db_dev_poleluxe > ~/.sql/db_dev_poleluxe_schema_2020-01-09.sql;
```

```
(app-server) $ mysqldump \
    -u root \
    -p \
    --port=3306 \
    --single-transaction \
    --routines \
    --triggers \
    --databases db_dev_poleluxe > ~/.sql/db_dev_poleluxe_2020-01-09.sql;
```

**Downloading cms-database sql file**

```
$ scp -i ~/.pems/albert-stg.pem ubuntu@ec2-api-stg.dailytrainingpartner.com:/home/ubuntu/app/dtpcmsdb_stg.sql ~/.sql

$ scp -i ~/.pems/albert-stg.pem ubuntu@api-server.public.ip:/home/ubuntu/app/dtpcmsdb.sql ~/.sql
$ scp -i ~/.pems/albert-stg.pem ubuntu@api-server.public.ip:/home/ubuntu/app/db_prod_poleluxe.sql ~/.sql

$ scp -i ~/.pems/albert-stg.pem ubuntu@worker-server.public.ip:/home/ubuntu/app/db_prod_poleluxe.sql ~/.sql
$ scp -i ~/.pems/albert-stg.pem ubuntu@worker-server.public.ip:/home/ubuntu/app/db_app_prod_poleluxe.sql ~/.sql
```

### Prepare empty databases to apply dump file in local environment

```
mysql> show databases;
+----------------------------------+
| Database                         |
+----------------------------------+
| information_schema               |
| db_dev_analytics_v1_test         |
| db_app_dev_poleluxe              |
| db_dev_poleluxe                  |
| db_stg_analytics_v1_test         |
| db_app_stg_poleluxe              |
| db_stg_poleluxe                  |
| mysql                            |
| performance_schema               |
| test_analytics_db                |
| test_db_app_dev_poleluxe_v1_test |
| test_db_app_stg_poleluxe_v1_test |
| test_db_dev_analytics_v1_test    |
| test_db_dev_poleluxe_v1_test     |
| test_db_stg_poleluxe_v1_test     |
| test_titdb                       |
+----------------------------------+
16 rows in set (0.02 sec)

mysql> drop database db_dev_analytics_v1_test;
Query OK, 4 rows affected (0.10 sec)

mysql> drop database db_app_dev_poleluxe;
Query OK, 14 rows affected (0.12 sec)

mysql> drop database db_dev_poleluxe;
Query OK, 104 rows affected (0.75 sec)

mysql> drop database db_stg_analytics_v1_test;
Query OK, 4 rows affected (0.02 sec)

mysql> drop database db_app_stg_poleluxe;
Query OK, 14 rows affected (0.09 sec)

mysql> drop database db_stg_poleluxe;
Query OK, 104 rows affected (0.76 sec)

mysql> show databases;
+----------------------------------+
| Database                         |
+----------------------------------+
| information_schema               |
| mysql                            |
| performance_schema               |
| test_analytics_db                |
| test_db_app_dev_poleluxe_v1_test |
| test_db_app_stg_poleluxe_v1_test |
| test_db_dev_analytics_v1_test    |
| test_db_dev_poleluxe_v1_test     |
| test_db_stg_poleluxe_v1_test     |
| test_titdb                       |
+----------------------------------+
10 rows in set (0.01 sec)

mysql> create database db_dev_analytics_v1_test;
Query OK, 1 row affected (0.00 sec)

mysql> create database db_app_dev_poleluxe;
Query OK, 1 row affected (0.00 sec)

mysql> create database db_dev_poleluxe;
Query OK, 1 row affected (0.00 sec)

mysql> create database db_stg_analytics_v1_test;
Query OK, 1 row affected (0.00 sec)

mysql> create database db_app_stg_poleluxe;
Query OK, 1 row affected (0.00 sec)

mysql> create database db_stg_poleluxe;
Query OK, 1 row affected (0.00 sec)

mysql> show databases;
+----------------------------------+
| Database                         |
+----------------------------------+
| information_schema               |
| db_dev_analytics_v1_test         |
| db_app_dev_poleluxe              |
| db_dev_poleluxe                  |
| db_stg_analytics_v1_test         |
| db_app_stg_poleluxe              |
| db_stg_poleluxe                  |
| mysql                            |
| performance_schema               |
| test_analytics_db                |
| test_db_dev_app_poleluxe_v1_test |
| test_db_stg_app_poleluxe_v1_test |
| test_db_dev_analytics_v1_test    |
| test_db_dev_poleluxe_v1_test     |
| test_db_stg_poleluxe_v1_test     |
| test_titdb                       |
+----------------------------------+
16 rows in set (0.00 sec)
```

### Importing Data from a dump file in local environment

(1)	Create new database via mysql environment. Use the databasename specified from **.env** file settings. In this case database name is `db_dev_poleluxe`. If database already existed you need to delete and recreate it from scratch.
	
**.env (api)**
	
```
DATABASE_URL=mysql://root:password@localhost:3306/db_dev_poleluxe
DATABASE_URL_CMS=mysql://root:password@localhost:3306/db_app_dev_poleluxe
DATABASE_URL_ANALYTICS=mysql://root:password@localhost:3306/db_dev_analytics_v1_test?charset=utf8
```

**.env (app-server)**
	
```
DATABASE_URL=mysql://root:password@localhost:3306/db_dev_app_poleluxe
DATABASE_URL_CMS=mysql://root:password@localhost:3306/db_dev_poleluxe
DATABASE_URL_ANALYTICS=mysql://root:password@localhost:3306/db_dev_analytics_v1_test?charset=utf8
```
	
**mysql shell**
	
```
$ mysql -u root -p
mysql> create database db_stg_poleluxe;
mysql> create database db_app_stg_poleluxe;
mysql> create database db_stg_analytics_v1_test;
```
```
$ mysql -u root -p
mysql> create database db_dev_poleluxe;
mysql> create database db_app_dev_poleluxe;
mysql> create database db_dev_analytics_v1_test;
```

```
$ docker exec -it app-server_mysql_1 /bin/sh
sh-4.2# mysql -u root -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 1607
Server version: 5.6.43 MySQL Community Server (GPL)
...
mysql> 
```

(2) Open and prepare the dumpfile **(dtpcmsdb_stg.sql)** and **change current databasename** as described above

```
mysql> CREATE DATABASE db_dev_poleluxe;
mysql> USE db_dev_poleluxe;
```
	
(3) **Import the dumpfile** to target database using this command

```
(tt-django-albert) $ mysql -u root db_dev_poleluxe < ~/.sql/dtpcmsdb_yyyy-mm-dd.sql -p

(tt-django-albert) $ mysql -u root db_prod_poleluxe < ~/.sql/db_prod_poleluxe_yyyy-mm-dd.sql -p
(tt-django-albert) $ mysql -u root db_dev_poleluxe < ~/.sql/db_prod_poleluxe_yyyy-mm-dd.sql -p
(tt-django-albert) $ mysql -u root db_stg_poleluxe < ~/.sql/db_prod_poleluxe_yyyy-mm-dd.sql -p

(tt-django-albert) $ mysql -u root db_app_prod_poleluxe < ~/.sql/db_app_prod_poleluxe_yyyy-mm-dd.sql -p
(tt-django-albert) $ mysql -u root db_app_dev_poleluxe < ~/.sql/db_app_prod_poleluxe_yyyy-mm-dd.sql -p
(tt-django-albert) $ mysql -u root db_app_stg_poleluxe < ~/.sql/db_app_prod_poleluxe_yyyy-mm-dd.sql -p
```

```
$ (tt-app-server) docker exec -i app-server_mysql_1 /usr/bin/mysql -u root db_app_dev_poleluxe < ~/.sql/db_app_dev_poleluxe_yyyy-mm-dd.sql --password=password
$ (tt-app-server) docker exec -i app-server_mysql_1 /usr/bin/mysql -u root db_app_stg_poleluxe < ~/.sql/db_app_stg_poleluxe_yyyy-mm-dd.sql --password=password

$ (tt-app-server) docker exec -i app-server_mysql_1 /usr/bin/mysql -u root db_dev_poleluxe < ~/.sql/db_dev_poleluxe_yyyy-mm-dd.sql --password=password
$ (tt-app-server) docker exec -i app-server_mysql_1 /usr/bin/mysql -u root db_stg_poleluxe < ~/.sql/db_stg_poleluxe_yyyy-mm-dd.sql --password=password

$ (tt-api) docker exec -i api_mysql_1 /usr/bin/mysql -u root db_app_dev_poleluxe < ~/.sql/db_dev_poleluxe_yyyy-mm-dd.sql --password=password
$ (tt-api) docker exec -i api_mysql_1 /usr/bin/mysql -u root db_app_stg_poleluxe < ~/.sql/db_stg_poleluxe_yyyy-mm-dd.sql --password=password

$ (tt-api) docker exec -i api_mysql_1 /usr/bin/mysql -u root db_dev_poleluxe < ~/.sql/db_dev_poleluxe_yyyy-mm-dd.sql --password=password
$ (tt-api) docker exec -i api_mysql_1 /usr/bin/mysql -u root db_stg_poleluxe < ~/.sql/db_stg_poleluxe_yyyy-mm-dd.sql --password=password
```
	
(4) Perform the migrations to migrate missing items

`(tt-cms) $ ./manage.py migrate`
	
`(tt-app-server) $ ./manage.py migrate`
	
```
(tt-api) $ ./manage.py migrate
(tt-api) $ ./manage.py migrate --database=analytics_db
```
	
### Import the existing datasource file to local database

```
$ ./manage.py loaddata app_language.json
$ ./manage.py loaddata cheer_up_message.json
$ ./manage.py loaddata level_points.json
```

### Export data needed for user model from remote server

```
$ ./manage.py dumpdata auth.user --indent 4 > auth_user.json --settings=PoleLuxeProject.settings
$ ./manage.py dumpdata PoleLuxe.app --indent 4 > app.json --settings=PoleLuxeProject.settings
$ ./manage.py dumpdata PoleLuxe.company --indent 4 > company.json --settings=PoleLuxeProject.settings
$ ./manage.py dumpdata PoleLuxe.userdepartment --indent 4 > user_department.json --settings=PoleLuxeProject.settings
$ ./manage.py dumpdata PoleLuxe.usergroup --indent 4 > user_group.json --settings=PoleLuxeProject.settings
$ ./manage.py dumpdata PoleLuxe.userjobposition --indent 4 > user_job_position.json --settings=PoleLuxeProject.settings
$ ./manage.py dumpdata PoleLuxe.user --indent 4 > user.json --settings=PoleLuxeProject.settings
```

```
./manage.py dumpdata
	auth.user
	PoleLuxe.app
	PoleLuxe.company
	PoleLuxe.userdepartment
	PoleLuxe.usergroup
	PoleLuxe.userjobposition
	PoleLuxe.user
		--indent 4 > all.json
		--settings=PoleLuxeProject.settings
```

### Download exported data from remote server

```
$ scp -i albert-stg.pem ubuntu@ec2-api-stg.dailytrainingpartner.com:/home/ubuntu/dtpapi/current/auth_user.json ~/.sql
$ scp -i albert-stg.pem ubuntu@ec2-api-stg.dailytrainingpartner.com:/home/ubuntu/dtpapi/current/app.json ~/.sql
$ scp -i albert-stg.pem ubuntu@ec2-api-stg.dailytrainingpartner.com:/home/ubuntu/dtpapi/current/company.json ~/.sql
$ scp -i albert-stg.pem ubuntu@ec2-api-stg.dailytrainingpartner.com:/home/ubuntu/dtpapi/current/user_department.json ~/.sql
$ scp -i albert-stg.pem ubuntu@ec2-api-stg.dailytrainingpartner.com:/home/ubuntu/dtpapi/current/user_group.json ~/.sql
$ scp -i albert-stg.pem ubuntu@ec2-api-stg.dailytrainingpartner.com:/home/ubuntu/dtpapi/current/user_job_position.json ~/.sql
$ scp -i albert-stg.pem ubuntu@ec2-api-stg.dailytrainingpartner.com:/home/ubuntu/dtpapi/current/user.json ~/.sql
```

### Import the downloaded datasource file to local database

```
$ ./manage.py loaddata ~/.sql/auth_user.json
$ ./manage.py loaddata ~/.sql/app.json
$ ./manage.py loaddata ~/.sql/company.json
$ ./manage.py loaddata ~/.sql/user_department.json
$ ./manage.py loaddata ~/.sql/user_group.json
$ ./manage.py loaddata ~/.sql/user_job_position.json
$ ./manage.py loaddata ~/.sql/user.json
```

```
$ ./manage.py loaddata ~/.sql/*.json
```

## Switching databases between development and staging

We assumed databases in development environment has different schema from staging.

### @ DEVELOPMENT virtualenv and settings

**.env** (api), (cms) 

```
DATABASE_URL=mysql://root:password@localhost:3306/db_dev_poleluxe
DATABASE_URL_CMS=mysql://root:password@localhost:3306/db_app_dev_poleluxe
DATABASE_URL_ANALYTICS=mysql://root:password@localhost:3306/db_dev_analytics_v1_test?charset=utf8
```

**.env** (app-server)

```
DATABASE_URL=mysql://root:password@localhost:3306/db_app_dev_poleluxe
DATABASE_URL_CMS=mysql://root:password@localhost:3306/db_dev_poleluxe
DATABASE_URL_ANALYTICS=mysql://root:password@localhost:3306/db_dev_analytics_v1_test?charset=utf8
```

**.env_test** (api) 

```
DATABASE_URL=mysql://root:password@localhost:3306/db_dev_poleluxe_v1_test?charset=utf8
DATABASE_URL_ANALYTICS=mysql://root:password@localhost:3306/analytics_db?charset=utf8
```

**docker-compose.yml** (app-server)

```
services:
  mysql:
    image: mysql/mysql-server:5.6
    command: mysqld --character-set-server=utf8 --collation-server=utf8_unicode_ci
    environment:
      MYSQL_ROOT_PASSWORD: 'password'
      MYSQL_ROOT_HOST: '%'
      MYSQL_DATABASE: 'db_app_dev_poleluxe'
    restart: 'always'
    ...
```

**bitbucket-pipelines.yml** (app-server)

```
definitions:
  services:
    mysql:
      image: mysql:5.6
      command: mysqld --character-set-server=utf8 --collation-server=utf8_unicode_ci --init-connect='SET NAMES UTF8;'
      environment:
        MYSQL_DATABASE: 'db_app_dev_poleluxe'
        MYSQL_ROOT_PASSWORD: 'password'
    ...
```

### @ STAGING virtualenv and settings

**.env** (api), (cms)

```
DATABASE_URL=mysql://root:password@localhost:3306/db_stg_poleluxe
DATABASE_URL_CMS=mysql://root:password@localhost:3306/db_app_stg_poleluxe
DATABASE_URL_ANALYTICS=mysql://root:password@localhost:3306/db_stg_analytics_v1_test?charset=utf8
```

**.env** (app-server)

```
DATABASE_URL=mysql://root:password@localhost:3306/db_app_stg_poleluxe
DATABASE_URL_CMS=mysql://root:password@localhost:3306/db_stg_poleluxe
DATABASE_URL_ANALYTICS=mysql://root:password@localhost:3306/db_stg_analytics_v1_test?charset=utf8
```

**.env_test** (api) 

```
DATABASE_URL=mysql://root:password@localhost:3306/db_stg_poleluxe_v1_test?charset=utf8
DATABASE_URL_ANALYTICS=mysql://root:password@localhost:3306/analytics_db?charset=utf8
```

**docker-compose.yml** (app-server)

```
services:
  mysql:
    image: mysql/mysql-server:5.6
    command: mysqld --character-set-server=utf8 --collation-server=utf8_unicode_ci
    environment:
      MYSQL_ROOT_PASSWORD: 'password'
      MYSQL_ROOT_HOST: '%'
      MYSQL_DATABASE: 'db_app_stg_poleluxe'
    restart: 'always'
    ...
```

**bitbucket-pipelines.yml** (app-server)

```
definitions:
  services:
    mysql:
      image: mysql:5.6
      command: mysqld --character-set-server=utf8 --collation-server=utf8_unicode_ci --init-connect='SET NAMES UTF8;'
      environment:
        MYSQL_DATABASE: 'db_app_stg_poleluxe'
        MYSQL_ROOT_PASSWORD: 'password'
    ...
```

### Upgrade to selected environment

```
(tt-django-albert) $ python setup.py sdist
```

**IMPORTANT:** This will create a common library file like `django-albert-0.11.6.tar.gz` to pip install in other servers. The filename was configured in `setup.py`.

Apply upgrade to the following virtualenv `tt-cms`, `tt-app-server`, `tt-api`

```
$ pip install /william/_git/thinkittwice/django-albert/dist/django-albert-0.11.6.tar.gz --upgrade
```