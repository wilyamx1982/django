# ModelViewSet

1. Getting the `authenticated_user`

		def validate_white_list_user_group(self, obj):
		    request = self.context.get('request')
		    if request and hasattr(request, 'authenticated_user'):
		        default_company_manager_helper.validate_user_group_listing(
		            obj, request.authenticated_user.id
		        )
				    return obj
		    
1. Adding optional **include** params to context

		def get_serializer_context(self):
		    context = super(FeedViewSet, self).get_serializer_context()
		    if 'include' in self.request.query_params:
		            context.update({
		                'include': self.request.query_params['include']
		            })
            
