# Debugging

## Monitoring Logs

```
(titworker) ubuntu@ip-10-0-1-227:~/titworker/current/logs$ tail -f error.log -n 200
(titworker) ubuntu@ip-10-0-1-227:~/titworker/current/logs$ tail -f rqworker_supervisor.log -n 200
(titworker) ubuntu@ip-10-0-1-227:~/titworker/current/logs$ tail -f gunicorn_supervisor.log -n 200
```

**Docker logs**

```
ubuntu@ip-10-0-19-123:~$ docker logs -f app_rq_1 (follow log output / tailing)

ubuntu@ip-10-0-19-123:~$ docker logs -f app_web_1 (follow log output / tailing)
ubuntu@ip-10-0-19-123:~$ docker logs -f app_web_1 | grep 'DELETE /api/apns'
ubuntu@ip-10-0-19-123:~$ docker logs -f app_web_1 | grep 'POST /api/v1/memsource-events'
ubuntu@ip-10-0-19-123:~$ docker logs -f app_web_1 | grep 'errors'

ubuntu@ip-10-0-19-123:~$ docker logs -f app_web_1 -t --since 2020-05-12 | grep error
ubuntu@ip-10-0-19-123:~$docker logs app_web_1 -t --since 2020-05-12 | grep -E  "2020-05-12&error"
ubuntu@ip-10-0-19-123:~$ docker logs app_web_1 -t --since 2019-05-28T05:00:00 --until 2019-05-28T10:00:00
ubuntu@ip-10-0-19-123:~$ docker logs app_app_1 -t --since 2019-05-28T05:00:00 --until 2019-05-28T10:00:00
```

**Django log files**

```
ubuntu@ip-10-0-18-81:~$ ls app/logs
cron.log  debug.log  error.log

ubuntu@ip-10-0-18-81:~$ cat app/logs/cron.log
ubuntu@ip-10-0-18-81:~$ cat app/logs/error.log

ubuntu@ip-10-0-1-105:~$ tail -f app/logs/debug.log -n 200 | grep -E 'tasks|Device Push'
```

**Other system log locations**

```
ubuntu@ip-10-0-18-81:~$ ls /var/log
alternatives.log  apt       btmp                   cloud-init.log  dpkg.log  kern.log  lxd     syslog.1     unattended-upgrades
amazon            auth.log  cloud-init-output.log  dist-upgrade    fsck      lastlog   syslog  syslog.2.gz  wtmp

ubuntu@ip-10-0-18-81:~$ grep cron /var/log/syslog

ubuntu@ip-10-0-18-81:~$ tail -f /var/log/syslog | grep docker
ubuntu@ip-10-0-18-81:~$ tail -f /var/log/syslog | grep cron
```

## How to update debug settings (env file) from host machine and apply in the running apps from remote server?

	# - - - - - - - - - - - - - - - - - - - - - -
	# host machine is outside docker container
	# - - - - - - - - - - - - - - - - - - - - - -
	ubuntu@ip-10-0-1-74:~/app$ ls
	certificates  docker-compose.yml  job.sh  logs  media  static
	ubuntu@ip-10-0-1-74:~/app$ vim .env
	
	ubuntu@ip-10-0-1-74:~/app$ docker-compose restart rq
	Restarting app_rq_1 ... done
	ubuntu@ip-10-0-1-74:~/app$ docker-compose restart app
	Restarting app_app_1 ... done

	# IMPORTANT:
	# The running app will get the updated .env file
	# from the host machine after restarted
	
	ubuntu@ip-10-0-1-59:~$ cat ~/app/.env
	ubuntu@ip-10-0-1-59:~$ vim ~/app/.env
	
	# - - - - - - - - - - - - - - - - - - - - - -
	# verification
	# - - - - - - - - - - - - - - - - - - - - - -
	ubuntu@ip-10-0-2-129:~$ cd app
	
	ubuntu@ip-10-0-2-129:~/app$ docker exec -it app_app_1 /bin/bash
	root@f3520135f820:/app# cat .env

	ubuntu@ip-10-0-2-129:~/app$ docker exec -it app_rq_1 /bin/bash
	root@f3520135f820:/app# cat .env
	

**IMPORTANT:**

* **Server Error (500) will be reported from [Sentry](https://sentry.io/think-it-twice/).** Get the details from there.
* **`docker-compose restart` the app service** to apply changes to .env file

## How to revert temporary code changes from live remote server

	ubuntu@ip-10-0-1-235:~/app$ ls
	certificates  docker-compose.yml  job.sh  logs  media  static
	ubuntu@ip-10-0-1-235:~/app$ docker-compose down
	Stopping app_web_1 ... done
	Stopping app_rq_1  ... done
	Stopping app_app_1 ... done
	Removing app_web_1 ... done
	Removing app_rq_1  ... done
	Removing app_app_1 ... done
	Removing network app_default
	ubuntu@ip-10-0-1-235:~/app$ docker ps
	CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
	ubuntu@ip-10-0-1-235:~/app$ docker-compose up -d
	Creating network "app_default" with the default driver
	Creating app_app_1 ... done
	Creating app_rq_1  ... done
	Creating app_web_1 ... done

## Running Test Suite

### API server : Makefile (api)

1. Install `cmake`

1. Run the test suite

	**Isolation testing**
	* Locate the file [**Makefile (api)**] then open to edit
	* **Specify the target** test from **line 84** like

	`./manage.py test api.v1.views.tests.test_users` or 
	`./manage.py test api.v1.views.tests.test_users.UserTestCase.test_create_unique`

    ```
    (tt-api) $ make test
    ```
    
### Django-albert server : Makefile (django-albert)
 
	(tt-django-albert) $ make test
	(tt-django-albert) $ make test ENV=pipelines
    
## Troubleshooting and Debugging Tips

1. When encountered status **500: Internal Server Error to Swagger**, error details can be found in **api/logs/error.log**.

1. When encountered **failed tests in unit testing**, error details can be found in **/logs/error.log**.

1. Deleting all .pyc files will fix **Bad Magic Number error**. Let the interpreter re-compile the py files.

		$ find . -name "*.pyc" -delete

1. **Login using swagger without the django-rq and app-server running.** In order to try the `/api/login` or `/api/logout` endpoint (**api/v1/view/auth.py**) using Swagger, advised to comment out the code from `push_notification.delete_devices(user.id)` and insert `pass` to avoid errors. You will use the **token response** from this endpoint for the required parameter for **X-Auth-Token** to try the other endpoints.

	**auth.py**
		
		@api_view(['POST'])
		@exceptions_catched
		def login(request):
		    ...
		        if user.is_login:
		            """
		            User is already logged in with other device so we'll delete tokens
		            of that device.
		            """
		            # push_notification.delete_devices(user.id)
		            pass
		
	**authentication.py**
		
		def clear_credentials(self, user):
			user.uuid = None
			user.token = None
			user.is_login = False
			user.save()
			# push_notification.delete_devices(user.id)
		
	**push_notification.py**
		
		def delete_devices(name):
			return
			for _, url in settings.PUSH_NOTIFICATION_DEVICE_API_URI.iteritems():
				helper = PushNotificationDeviceHelper(url)
			
			    res = helper.delete(name=name)
			
			    if res.status_code >= 400:
			    	raise Exception('Exception: ' + res.json())

1. **Clear migration history.** You want to clear all the migration history but you want to keep the existing database.

		$ ./manage.py showmigrations
		$ ./manage.py migrate --fake PoleLuxe zero
		$ ./manage.py migrate --fake
		$
		$ find . -path "*/migrations/*.py" -not -name "__init__.py" -delete
		$ find . -path "*/migrations/*.pyc"  -delete

1. **App keep installing the old-version of django-albert** when `pip install`. Manually uninstall the django-albert module and install again to apply updated version for django-albert module.

	**Error message**
	
		* ImportError: cannot import name RemovedInDjango19Warning ...
	
		(tt-django-albert) $ pip uninstall django-albert
		Uninstalling django-albert-0.11.6:
		  Would remove:
		    /Users/wrena/.venvs/tt-django-albert/lib/python2.7/site-packages/PoleLuxe/*
		    /Users/wrena/.venvs/tt-django-albert/lib/python2.7/site-packages/django_albert-0.11.6.dist-info/*
		Proceed (y/n)? y
		  Successfully uninstalled django-albert-0.11.6
		
		(tt-django-albert) $ pip uninstall django
		...
		Proceed (y/n)? y
		  Successfully uninstalled Django-1.11.14
		
		(tt-django-albert) $ pip install -r requirements/base.txt --upgrade

1. **Wrong pip directory use.** In everytime do `pip install` it **keeps using the system pip directory instead from the virtual env**. Solution is to recreate and configure the virtualenv again.

		$ rmvirtualenv tt-api
		
		# create separate virtualenv and assign working directory
		$ mkvirtualenv ~/.venvs/tt-api --python=python
		
		$ workon tt-api
		(tt-api) $ setvirtualenvproject $VIRTUAL_ENV /william/_git/thinkittwice/api
		(tt-api) $ setvirtualenvproject $VIRTUAL_ENV /users/shared/relocated_items/security/william/_git/thinkittwice/api
		
1. **Error after pip install to requirement**. Solution is to check the python and pip directory. **Re-apply workon to virtualen**.

		(tt-django-albert) $ workon tt-django-albert
		(tt-django-albert) $ which python
		/Users/wrena/.venvs/tt-django-albert/bin/python
		(tt-django-albert) $ which pip
		/Users/wrena/.venvs/tt-django-albert/bin/pip

1. **Error after applied showmigrations.** Solution is to copy the source file **libmysqlclient.18.dylib** to virtual environment folder.

		(tt-django-albert) $ open /usr/local/cellar/mysql@5.6/5.6.46_2/lib
		(tt-django-albert) $ cp /usr/local/cellar/mysql@5.6/5.6.46_2/lib/libmysqlclient.dylib $PWD
		
		(tt-django-albert) $ cp /usr/local/opt/mysql@5.6/lib/libmysqlclient.dylib $PWD
		
1. **Error that involves to install mysqlclient or MYSQL-python in target pipelinetest failed.** Solution is to **clear the cache from bitbucket branch**.

	**Error message**
	
		...
		Did you install mysqlclient or MySQL-python?
		Makefile:2: recipe for target 'pipelinestest' failed
		make: *** [pipelinestest] Error 1
		
1. **Error for ECR login** in local environment. **Solution is to re-install awscli**.

	**Error message**
	
		(tt-api3) $ $(aws ecr get-login --no-include-email --region us-west-1)
		Credential named assume-role-with-web-identity not found. 
		
	**Reinstallation**
	
		(tt-api3) $ pip uninstall awscli
		(tt-api3) $ pip install awscli --upgrade
		(tt-api3) $ aws --version
		aws-cli/1.18.49 Python/3.7.6 Darwin/19.4.0 botocore/1.15.49
		
		(tt-api3) $ $(aws ecr get-login --no-include-email --region us-west-1)
		WARNING! Using --password via the CLI is insecure. Use --password-stdin.
		Login Succeeded
		