# General Instructions

## Python Installation

[Download here](https://www.python.org/downloads/)

[Python 3.7.6 - Dec. 18, 2019 for macOS 64-bit installer](https://www.python.org/ftp/python/3.7.6/python-3.7.6-macosx10.9.pkg)

## Pipenv Installation

[Pipenv Video Tutorial](https://www.youtube.com/watch?v=zDYL22QNiWk)

	% pip3 install pipenv
	
	% cd ~/.venvs
	% cd project1
	% pwd
	/Users/william.rena/.venvs/project1
	
	project 1 % pipenv --python 3.9
	

## How to pip install for python2 and python3

	$ python3 -m pip install -U --force-reinstall pip
	$ python -m pip install -U --force-reinstall pip


## How to install MySQL in MacOSX using [Homebrew](https://brew.sh/)

Cheatsheet links:

* [MySQL Tutorial](http://www.mysqltutorial.org/mysql-cheat-sheet.aspx)
* [Jumping Monkey](http://reference.jumpingmonkey.org/database/mysql/cheatsheet.html)

```
# find all running mysql processes
$ ps aux | grep mysql56

$ brew uninstall mysql@5.6
$ brew install mysql@5.6
$ brew reinstall mysql@5.6
$ brew list

$ which python
/usr/bin/python
$ pip install MySQL-python

$ mysql --version
mysql  Ver 14.14 Distrib 5.6.42, for osx10.14 (x86_64) using  EditLine wrapper

$ brew services list
Name      Status  User  Plist
mysql@5.6 started wrena /Users/wrena/Library/LaunchAgents/homebrew.mxcl.mysql@5.6.plist
$ brew services stop mysql56
$ brew services start mysql56
$ brew services restart mysql@5.6
$ brew info mysql@5.6

$ /usr/local/opt/mysql@5.6/bin/mysql.server start
$ /usr/local/opt/mysql@5.6/bin/mysql.server status
SUCCESS! MySQL running (39776)
$ /usr/local/opt/mysql@5.6/bin/mysql_secure_installation (add-root-password)
$ /usr/local/opt/mysql@5.6/bin/mysql.server restart
$ /usr/local/opt/mysql@5.6/bin/mysql.server stop

# - - - - - - - - - - - - -
# RE-INSTALLATION PROCESS
# - - - - - - - - - - - - -
$ source ~/.bash_profile
$
$ sudo ln -s /usr/local/cellar/mysql@5.6/5.6.46_2/lib/libmysqlclient.18.dylib (installation-required)
$ /usr/local/opt/mysql@5.6/5.6.46_2/bin/mysql_secure_installation (add-root-password)
$
$ sudo ln -s /usr/local/opt/mysql@5.6/lib/libmysqlclient.18.dylib (installation-required) ***
$ /usr/local/opt/mysql@5.6/bin/mysql_secure_installation (add-root-password)

$ brew unlink mysql (if-necessary)
```

**IMPORTANT:**

* Always check the directory of the **installed mysql version** especially when upgrading.
* You can copy **libmysqlclient.dylib** from the repo folder if cannot be install using command.

```
$ /usr/local/cellar/mysql@5.6/5.6.42/bin/mysql -u root
$ /usr/local/cellar/mysql@5.6/5.6.42/bin/mysql -u root -p
$ /usr/local/cellar/mysql@5.6/5.6.42/bin/mysql -u root -p db_dev_poleluxe
$ /usr/local/cellar/mysql@5.6/5.6.42/bin/mysql -u root -p drop database db_dev_poleluxe

[mysql> create database db_dev_poleluxe;
[mysql> drop database db_dev_poleluxe;
[mysql> use db_dev_poleluxe;
Database changed

$ which mysql_config
/usr/local/opt/mysql@5.6/bin/mysql_config

# short version
$ mysql -u root -p
$ mysql -u root -p db_dev_poleluxe
$ mysql -u root -p drop database db_dev_poleluxe

[mysql> show databases;
[mysql> select database();
+---------------------+
| database()          |
+---------------------+
| db_dev_poleluxe |
+---------------------+
1 row in set (0.00 sec)

mysql> show tables;
+-----------------------------------------------+
| Tables_in_db_dev_poleluxe                     |
+-----------------------------------------------+
| PoleLuxe_activity                             |
...
| PoleLuxe_luxuryculturetranslation             |
| PoleLuxe_media                                |
| PoleLuxe_media_likes                          |
| PoleLuxe_media_product_groups                 |
| PoleLuxe_mediacomment                         |
...
+-----------------------------------------------+
105 rows in set (0.00 sec)

mysql> describe PoleLuxe_media;
+--------------+--------------+------+-----+---------+----------------+
| Field        | Type         | Null | Key | Default | Extra          |
+--------------+--------------+------+-----+---------+----------------+
| id           | int(11)      | NO   | PRI | NULL    | auto_increment |
| title        | varchar(100) | NO   |     | NULL    |                |
| content      | longtext     | NO   |     | NULL    |                |
| created_at   | datetime     | NO   |     | NULL    |                |
| updated_at   | datetime     | NO   |     | NULL    |                |
| user_id      | int(11)      | NO   | MUL | NULL    |                |
| type         | int(11)      | NO   |     | NULL    |                |
| is_active    | tinyint(1)   | YES  |     | NULL    |                |
| publish_date | date         | YES  |     | NULL    |                |
+--------------+--------------+------+-----+---------+----------------+
9 rows in set (0.02 sec)
```

```
$ brew services list
$ brew services start mysql56
$ brew services restart mysql56
$ brew services stop mysql56
```

## MySQL Workbench

**MySQL Workbench** is developed and tested for MySQL Server versions **5.6**, **5.7** and **8.0**

[MySQL Community Downloads](https://dev.mysql.com/downloads/)

## How to install [Redis in MacOSX](https://gist.github.com/tomysmile/1b8a321e7c58499ef9f9441b2faa0aa8) using Homebrew

	$ brew install redis
	$ brew services start redis
	$ brew services list
	Name      Status  User  Plist
	mysql@5.6 started wrena /Users/wrena/Library/LaunchAgents/homebrew.mxcl.mysql@5.6.plist
	redis     started wrena /Users/wrena/Library/LaunchAgents/homebrew.mxcl.redis.plist
	$ redis-cli ping
	PONG

## How to install [GoLang](https://github.com/minio/cookbook/blob/master/docs/how-to-install-golang.md) using Homebrew

	$ brew install git 
	$ brew install go
	$ source ~/.bash_profile
	$ go env
	$ go version
	
GoLang: `~/.bash_profile`

	$ export PATH=${HOME}/go/bin:$PATH

## Install [Django-RQ](https://github.com/rq/django-rq)

	(tt-app-server) $ pip install django-rq
	(tt-app-server) $ ./manage.py rqworker
	07:01:42 RQ worker 'rq:worker:Williams-Mac-mini.96521' started, version 0.13.0
	07:01:42 *** Listening on default...
	07:01:42 Cleaning registries for queue: default


## Create environment using [VirtualEnvWrapper](http://virtualenvwrapper.readthedocs.io/) in MacOSX

**python installation**

```
$ which python3
/Library/Frameworks/Python.framework/Versions/3.7/bin/python3
$ which pip3
/Library/Frameworks/Python.framework/Versions/3.7/bin/pip3
$ pip3 install virtualenvwrapper
Collecting virtualenvwrapper...
```

**~/.bash_profile**

```
# Setting PATH for Python 3.7
# The original version is saved in .bash_profile.pysave
PATH="/Library/Frameworks/Python.framework/Versions/3.7/bin:${PATH}"
export PATH

# virtualenvwrapper
export WORKON_HOME=~/.venvs
export VIRTUALENVWRAPPER_PYTHON=/Library/Frameworks/Python.framework/Versions/3.7/bin/python3.7
source /Library/Frameworks/Python.framework/Versions/3.7/bin/virtualenvwrapper.sh
```

**create virtualenv**

```
$ which python3
/Library/Frameworks/Python.framework/Versions/3.7/bin/python3

$ virtualenv -p python3 ~/.venvs
	created virtual environment CPython3.7.6.final.0-64 in 403ms
	creator CPython3Posix(dest=/Users/wrena/.venvs/tt-api, clear=False, no_vcs_ignore=False, global=False)
	seeder FromAppData(download=False, pip=bundle, setuptools=bundle, wheel=bundle, via=copy, app_data_dir=/Users/wrena/Library/Application Support/virtualenv)
added seed packages: pip==20.2.4, setuptools==50.3.2, wheel==0.35.1  activators BashActivator,CShellActivator,FishActivator,PowerShellActivator,PythonActivator,XonshActivator
```

**mkvirtualenv**

```
$ mkvirtualenv ~/.venvs/tt-api --python=python3
$ workon tt-api
(tt-api) $ setvirtualenvproject $VIRTUAL_ENV ~/_GIT/ThinkItTwice/api
$ workon tt-api
$ pwd
/Users/wrena/_GIT/ThinkItTwice/api
```

**more examples**

```
$ virtualenv -p python ~/.venvs/tt
$ source ~/.venvs/tt/bin/activate

$ sudo pip install flake8
$ sudo pip install virtualenvwrapper
$ which virtualenvwrapper.sh
/Users/wrena/.venvs/tt/bin/virtualenvwrapper.sh

# ~/.bash_profile
$ export WORKON_HOME=~/.venvs
$ export VIRTUALENVWRAPPER_PYTHON=~/.venvs/tt/bin/python
$ source ~/.venvs/tt/bin/virtualenvwrapper.sh

# ~/.bash_profile
$ export WORKON_HOME=~/.venvs
$ export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python
$ source /usr/local/bin/virtualenvwrapper.sh

# source
$ source ~/.bash_profile

# removing the virtual-env
$ rmvirtualenv tt-api

# create separate virtualenv and assign working directory
$ mkvirtualenv ~/.venvs/tt-api --python=python
$ mkvirtualenv ~/.venvs/tt-api3 --python=python3
$ mkvirtualenv ~/.venvs/tt-api3.6 --python=python3.6

$ workon tt-api
(tt-api) $ setvirtualenvproject $VIRTUAL_ENV /Users/wrena/Documents/_GIT/ThinkItTwice/api
(tt-api) $ setvirtualenvproject $VIRTUAL_ENV /william/_git/thinkittwice/api
(tt-api) $ setvirtualenvproject $VIRTUAL_ENV /users/shared/relocated_items/security/william/_git/thinkittwice/api

(tt-api) $ pwd
/william/_git/thinkittwice/api
/users/shared/relocated_items/security/william/_git/thinkittwice/api

# or change .project file
~/.venvs/tt-api/.project
```

**How to verify correct configurations?**

```
(tt-app-server) earvin-pc:app-server wrena$ pwd
/william/_git/thinkittwice/app-server

(tt-app-server) earvin-pc:app-server wrena$ which python
/Users/wrena/.venvs/tt-app-server/bin/python

(tt-app-server) earvin-pc:app-server wrena$ python --version
Python 2.7.10

(tt-app-server) earvin-pc:app-server wrena$ which pip
/Users/wrena/.venvs/tt-app-server/bin/pip

(tt-app-server) earvin-pc:app-server wrena$ pip --version
pip 18.1 from /Users/wrena/.venvs/tt-app-server/lib/python2.7/site-packages/pip (python 2.7)
```

**Suggested (virtual env : working directories / git repo)**

```
$ workon
tt-api
tt-app-server
tt-cms-spa
tt-cms
tt-django-albert

(tt-api) earvin-pc:api wrena$ pwd
/william/_git/thinkittwice/api

(tt-app-server) earvin-pc:app-server wrena$ pwd
/william/_git/thinkittwice/app-server

(tt-django-albert) earvin-pc:django-albert wrena$ pwd
/william/_git/thinkittwice/django-albert

(tt-cms) earvin-pc:cms wrena$ pwd
/william/_git/thinkittwice/cms

(tt-cms-spa) earvin-pc:cms-spa wrena$ pwd
/william/_git/thinkittwice/cms-spa
```

## Dashboard dependency installation guide in MacOSX

**Dependency server requirements**

1. (tt-app-server) redis-server
1. (tt-app-server) ./manage.py rqworker high default low
1. (tt-app-server) make run
1. (tt-api) make run 
1. (tt-cms-spa) npm start **cms-spa/src/env.json** should point to api-server

Edit **~/.bash_profile**

```
# nvm
export NODE_PATH="/usr/local/lib/node_modules"
export NVM_DIR="~/.nvm"
source $(brew --prefix nvm)/nvm.sh
```

**Installation commands**

```
$ mkdir ~/.nvm

(tt-cms-spa) $ brew uninstall nvm
(tt-cms-spa) $ brew install nvm
(tt-cms-spa) $ nvm install 6.14.4
(tt-cms-spa) $ nvm uninstall 6.14.4
(tt-cms-spa) $ nvm use --delete-prefix v6.14.4
Now using node v6.14.4 (npm v6.2.0)

(tt-cms-spa) $ nvm --version
0.33.11

(tt-cms-spa) $ npm -v
6.2.0

(tt-cms-spa) $ which node
~/.nvm/versions/node/v6.14.4/bin/node

(tt-cms-spa) $ nvm ls
(tt-cms-spa) $ nvm ls-remote
(tt-cms-spa) $ nvm ls-remote --lts

(tt-cms-spa) $ npm install -g bower
(tt-cms-spa) $ bower -v
1.8.4
```

**Running commands**

```
(tt-cms-spa) $ nvm --version
0.33.11

(tt-cms-spa) $ nvm use --delete-prefix v6.14.4
Now using node v6.14.4 (npm v6.2.0)

(tt-cms-spa) $ npm start
...
[11:22:51] Server started at http://127.0.0.1:8080
```

## ~/.bash_profile

```
# Setting PATH for Python 3.7
# The original version is saved in .bash_profile.pysave
PATH="/Library/Frameworks/Python.framework/Versions/3.7/bin:${PATH}"
export PATH

# mysql
export PATH=$PATH:/usr/local/cellar/mysql@5.6/5.6.46_2/bin
export DYLD_LIBRARY_PATH=/usr/local/cellar/mysql@5.6/5.6.46_2/lib/
export LDFLAGS='-I/usr/local/opt/openssl/include -L/usr/local/opt/openssl/lib'

# virtual env
export WORKON_HOME=~/.venvs
export VIRTUALENVWRAPPER_PYTHON=~/.venvs/tt/bin/python
source ~/.venvs/tt/bin/virtualenvwrapper.sh

# Nodejs
export PATH="/usr/local/opt/icu4c/bin:$PATH"
export PATH="/usr/local/opt/icu4c/sbin:$PATH"
export PATH="/usr/local/bin:$PATH"
export NODE_PATH="/usr/local/lib/node_modules"

# nvm
export NVM_DIR="~/.nvm"
source $(brew --prefix nvm)/nvm.sh
```

## AWS CLI

1. Install the [AWS CLI Using pip](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html#install-tool-pip) in **(tt-app-server)** and **(tt-api)**

		(tt-app-server) $ pip install awscli --upgrade
		(tt-app-server) $ pip uninstall awscli
		
		(tt-app-server) $ aws --version
		aws-cli/1.16.181 Python/2.7.10 Darwin/18.6.0 botocore/1.12.171

1. **Configure the AWS CLI** by defining [profiles](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html) in the [AWS configuration files](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)

	**~/.aws/credentials**
	
		[default]
		aws_access_key_id=*****PBDMRS37JD*****
		aws_secret_access_key=*****3AAgYfzSCwfLdyRDAIMCAaHpSCVzwJ*****
		
	**~/.aws/config**
	
		[default]
		region=us-west-1
		output=json
		
		[profile tit]
		region=us-west-1
		output=json
		
		[profile dtp]
		region=ap-southeast-1
		output=json

	**[get config settings](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)**
	
		(tt-app-server) $ aws configure get region --profile tit
		us-west-1   
		(tt-app-server) $ aws configure get region --profile dtp
		ap-southeast-1

1. **AWS CLI login**

		(tt-app-server) $ $(aws ecr get-login --no-include-email --region us-west-1)
		WARNING! Using --password via the CLI is insecure. Use --password-stdin.
		Login Succeeded
		
## Generate Certificates for Docker

1. **Install OpenSSL** via Homebrew

		(tt-app-server) $ brew install openssl
		(tt-app-server) $ brew upgrade openssl
		
1. **Create shared directories** for Docker

		(tt-app-server) $ sudo mkdir /etc/ssl/private
		(tt-app-server) $ sudo mkdir /etc/ssl/certificates
	
1. Generate the **certificates needed**

		(tt-app-server) $ sudo openssl req -x509 -newkey rsa:4096 -keyout /etc/ssl/private/ssl-cert-snakeoil.key -out /etc/ssl/certificates/ssl-cert-snakeoil.pem -days 365 -nodes
		(tt-app-server) $ sudo openssl dhparam -out /etc/ssl/certificates/dhparam.pem 2048
		
		