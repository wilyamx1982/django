# Mocking and Unit Testing

## Mocking requirements

#### (1) UserFactory

```
import requests_mock
from django.conf import settings

@requests_mock.mock()
def def test_list_search(self, m):
    m.post(settings.UNIQUE_VALIDATOR_ENDPOINT, json=True)

    UserFactory(
        name='Guido van Rossum',
        username='guido',
        email='guido.r@dropbox.com',
        active=True,
        company_id=self.user.company_id,
        user_group_id=self.user.user_group_id,
    )
```

#### (2) S3 Bucket

```
from moto import mock_s3_deprecated

@mock_s3_deprecated
def test_delete_not_owner_with_permission(self, m):
    self.bucket = self.create_s3_buckets()
```

#### (3) PUT/PATCH ImageField to Endpoint

```
import os
import tempfile
from PIL import Image

def test_update_with_permission(self):
    self.url = reverse('api-v1:luxuryculture-detail',
                       kwargs={'pk': self.luxury_culture.id})
    self.modelPermissionsSetup(LuxuryCulture, 'change')

    # existing image
    k = Key(self.bucket)
    k.key = self.temp_file.name
    self.assertTrue(k.exists())

    # new image
    image = Image.new('RGB', (100, 100))
    temp_file = tempfile.NamedTemporaryFile(suffix='.jpg')
    image.save(temp_file)

    self._upload_test_resources({
        temp_file.name: open(temp_file.name, 'rb')})

    data = {
        ...
        'featured_image_url': temp_file.name,
    }
    response = self.client.put(
        self.url,
        data=data,
        HTTP_X_AUTH_TOKEN=self.user.token,
    )
    self.assertEqual(status.HTTP_200_OK,
                     response.status_code)
    data.pop('featured_image_url')
    self.assertDictEqualRecursive(data, response.data)
    self.assertEqual(
        os.path.basename(temp_file.name),
        os.path.basename(response.data['featured_image_url'])
    )
    self.assertTrue(response.data['featured_image_url'].startswith(
        settings.AWS_S3_DOMAIN
    ))

    # existing image
    self.assertFalse(k.exists())

    # new image
    k = Key(self.bucket)
    k.key = temp_file.name
    self.assertTrue(k.exists())
```

```
class LuxuryCultureSerializer(
    serializers.ModelSerializer,
    WithExtractedImagePaths
):
    featured_image_url = serializers.CharField(
        max_length=250,
        validators=[S3FileValidator()],
        required=False
    )
```

#### (4) Apply [Faker](https://faker.readthedocs.io/en/latest/providers.html) Data

```
from faker import Faker

class TipsOfTheDayTest(BaseAPITestCase):
    """
    Tests for /api/v1/tipsofthedays/ endpoint.
    """
    def setUp(self):
        super(TipsOfTheDayTest, self).setUp()
        self.faker = Faker()
        ...
        
    def test_create_no_permission(self):
        self.url = reverse('api-v1:tipsoftheday-list')
        data = {
            'title': self.faker.catch_phrase(),
            ...
        }
        response = self.client.post(
            self.url,
            data=data,
            HTTP_X_AUTH_TOKEN=self.user.token,
        )
        self.assertEqual(
        	status.HTTP_403_FORBIDDEN,
          response.status_code
        )
```

**Common Provider Usage**

```
content = self.faker.sentence()
filename = self.faker.file_name(extension='json')
title = self.faker.catch_phrase()
tag = self.faker.word()

profile = self.faker.profile(fields=['username'])
user = UserFactory(username=profile['username'])
```

#### (5) POST param **as file**

```
import os
import tempfile

from PIL import Image

def test_create_with_permission_as_file(
        self,
        mock_post,
        mock_create_memory_source
):
    ...
    # new image
    image = Image.new('RGB', (100, 100))
    temp_file = tempfile.NamedTemporaryFile(suffix='.jpg')
    image.save(temp_file)

    with open(temp_file.name, 'rb') as image_file:
        data = {
            'image': image_file,
            ...
        }
        response = self.client.post(
            self.url,
            data=data,
            HTTP_X_AUTH_TOKEN=self.user.token,
        )
        self.assertEqual(status.HTTP_201_CREATED,
                         response.status_code)

        self.assertEqual(
            os.path.basename(temp_file.name),
            os.path.basename(response.data['image'])
        )
```

#### (6) POST param **as path**

```
import os
import tempfile

from boto.s3.key import Key
from moto import mock_s3_deprecated
from PIL import Image

@mock_s3_deprecated
def _upload_test_resources(self, resources):
    _, bucket = self.create_s3_buckets()
    for path, resource in resources.iteritems():
        resource.seek(0)
        k = Key(bucket)
        k.key = path
        k.set_contents_from_file(resource)
            
@override_settings(AWS_S3_DOMAIN='https://tit-test.s3.amazonaws.com/')
def test_create_featured_image_as_path(self):
    self.url = reverse('api-v1:luxuryculture-list')
    self.modelPermissionsSetup(LuxuryCulture, 'add')

    # new image
    image = Image.new('RGB', (100, 100))
    temp_file = tempfile.NamedTemporaryFile(suffix='.jpg')
    image.save(temp_file)

    self._upload_test_resources({
        temp_file.name: open(temp_file.name, 'rb')})

    data = {
        'featured_image_url': temp_file.name,
    }
    response = self.client.post(
        self.url,
        data=data,
        HTTP_X_AUTH_TOKEN=self.user.token,
    )
    self.assertEqual(
        status.HTTP_201_CREATED,
        response.status_code
    )
    data.pop('featured_image_url')
    self.assertDictEqualRecursive(data, response.data)
    self.assertEqual(
        os.path.basename(temp_file.name),
        os.path.basename(response.data['featured_image_url'])
    )
    self.assertTrue(response.data['featured_image_url'].startswith(
        settings.AWS_S3_DOMAIN
    ))

    # existing image
    k = Key(self.bucket)
    k.key = temp_file.name
    self.assertTrue(k.exists())
```

```
class LuxuryCultureSerializer(
    serializers.ModelSerializer,
    WithExtractedImagePaths
):
    featured_image_url = serializers.CharField(
        max_length=250,
        validators=[S3FileValidator()],
        required=False
    )
```

#### (7) Method and parameter calling assertion

```
def process_mem_source_info(
        self,
        memsource_path,
        memsource_target_languages,
        memsource_token,
        memsource_project_id
):
    with open(memsource_path, 'rb') as mem_source_json:
        mem_source_content = json.load(
            mem_source_json,
            encoding='utf-8'
        )

    for target_lang in memsource_target_languages:
        mem_source_content['targetLang'] = target_lang

        with open(memsource_path, 'wb') as mem_source_json:
            json.dump(mem_source_content,
                      codecs.getwriter('utf-8')(mem_source_json),
                      ensure_ascii=False)

        # - - - - - - - - - - - -
        # Memsource job create
        # - - - - - - - - - - - -

        default_memsource_helper.job_create(
            memsource_token,
            memsource_project_id,
            target_lang,
            open(memsource_path, 'rb')
        )

    if os.path.exists(memsource_path):
        os.remove(memsource_path)

    return mem_source_content
```

```
class MemsourceHelper(object):
    def job_create(self, token, project, target_language, json_file):
        api_url = settings.MEMSOURCE_JOB_CREATE_API
        response = requests.post(
            api_url,
            params={
                'token': token,
                'project': project,
                'targetLang': target_language,
                'json.htmlSubFilter': 'false',
            },
            files={
                'file': json_file
            }
        )
        ...
        return response
```

```
from mock import patch, call, ANY

@patch('api.v1.helpers.memsource.MemsourceHelper.job_create')
def test_memory_source_api_calls(
        self,
        mock_memsource_job_create
):
    """
    Calling memsource job-create
    """
    job_create_response = Response()
    job_create_response.status_code = status.HTTP_200_OK
    mock_memsource_job_create.return_value = job_create_response

    mock_token = '123456'
    self.with_memory_source_mixins.process_mem_source_info(
        self.mem_source_file_path,
        self.memsource_target_languages,
        mock_token,
        self.memsource_project_id
    )

    # - - - - - - - - - - - - - - - - - -
    # assert the job-create calls
    # - - - - - - - - - - - - - - - - - -

    request_calls = []

    for target_lang in self.memsource_target_languages:
        post_call = call(
            mock_token,
            self.memsource_project_id,
            target_lang,
            ANY
        )
        request_calls.append(post_call)

    mock_memsource_job_create.assert_has_calls(
        request_calls,
        any_order=False
    )
```
