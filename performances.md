# Performances

1. **Dictionary comprehension** - is a method for transforming one dictionary into another dictionary. During this transformation, items within the original dictionary can be conditionally included in the new dictionary and each item can be transformed as needed. 
1. use **list comprehension** instead of **map + lambda** 
1. If user is a **superuser** or **admin** it has all the permissions. 
1. **Prefer**

		product_group_ids_to_validate = Knowledge.objects.filter(
		    pk=obj.knowledge_id.id
		).values_list('product_group__id', flat=True)

	Instead of 
	
		product_group_ids_to_validate = map(
		    lambda i: i.id, obj.knowledge_id.product_group.all()
		)

1. **Prefer**

		if not serializer.Meta.model.objects.filter(
		    id=self.kwargs.get('pk')
		).exists():
			raise serializers.ValidationError(
			    '{} content {} does not exist!'.format(
			        serializer.Meta.model.__name__, self.kwargs.get('pk')
			    ))

    Instead of
    
		try:
			content = serializer.Meta.model.objects.get(id=self.kwargs.get('pk'))
		except serializer.Meta.model.DoesNotExist:
			raise serializers.ValidationError(
			    '{} content {} does not exist!'.format(
			        serializer.Meta.model.__name__, self.kwargs.get('pk')
			    )
			)

1. Do not rely always for request response data. Double check the created model using django shell. 
1. `print(‘text’, end=‘’)`

