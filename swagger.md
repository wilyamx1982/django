# Contents

* What is Swagger? [more details here](https://swagger.io/docs/specification/2-0/what-is-swagger/)
* Create swagger user
* Swagger in class-based views
* Swagger in function-based views

## What is Swagger?

**Swagger** allows you to **describe the structure of your APIs** so that machines can read them. The ability of APIs to **describe their own structure** is the root of all awesomeness in Swagger. Well, by reading your API’s structure, we can automatically build beautiful and **interactive API documentation**.

The **specification** asks you to include information like:

* What are all the **operations that your API supports**?
* What are your **API’s parameters and what does it return**?
* Does your API need some **authorization**?

## Using Swagger

Use the command below to create a user to allow [try-out] the apis within. **The user is not a PoleLuxe_user but a Django user type. User should perform first the `login api` to get the `token` needed in the rest of the request.**

```
(tt-api) $ ./manage.py setswaggeruser user-password
```

```
class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('password', type=str)

    def handle(self, *args, **options):
        try:
            user = User.objects.get(username='swagger')
        except ObjectDoesNotExist:
            user = User.objects.create_user(
                username='swagger',
                email='swagger@mailinator.com'
            )

        if user is not None:
            user.is_active = True
            user.is_staff = True
            user.set_password(options['password'])
            user.save()
```

```
(tt-api) $ make shell

Python 2.7.10 (default, Aug 17 2018, 17:41:52) 
[GCC 4.2.1 Compatible Apple LLVM 10.0.0 (clang-1000.0.42)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
(InteractiveConsole)
>>> from django.contrib.auth.models import User
>>> 
>>> User.objects.filter(email__icontains='@mailinator.com')
<QuerySet [<User: swagger>]>
>>> 
>>> user = User.objects.get(username='swagger')
>>> user.django_user.is_superuser = True
>>> user.save()
```

## Adding the Endpoint to SwaggerUI

### Adding schema for ModelViewSet (class-based views)

**IMPORTANT**

* Set schema using **AutoSchema** and verify it from swagger UI if parameters were detected automatically.
* Implement **manual_fields** if cannot be auto-detected the needed parameters.
* **location** values {**query, header, path, formData, body, form**}
* Required to use **filter_backends** for filtering and searching params.
* Remove the yaml docstring part after the use of schema is working.
* [OpenAPI](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#parameterObject)
* [Django Rest Framework Schemas](http://www.django-rest-framework.org/api-guide/schemas/)
* The following parent class that defined auto schema are: **BaseModelViewSet**, **CreateRetrieveListModelViewSet**. You don't need to set the schema if inheriting these classes.

from **/api/v1/views/**

```
class CompanyViewSet(BaseModelViewSet):
    """
    Manage company
    ---
    list:
        parameters:
        - name: X-Auth-Token
          required: true
          paramType: header
        - name: page
          required: false
          paramType: query
          description: Page number
        - name: page_size
          required: false
          paramType: query
          description: Number of records to fetch. (Default is 30, max of 100)
    create:
        parameters:
        - name: X-Auth-Token
          required: true
          paramType: header
    destroy:
        parameters:
        - name: X-Auth-Token
          required: true
          paramType: header
    retrieve:
        parameters:
        - name: X-Auth-Token
          required: true
          paramType: header
    update:
        parameters:
        - name: X-Auth-Token
          required: true
          paramType: header
    """
    schema = AutoSchema(
        manual_fields=[
            coreapi.Field(
                name="X-Auth-Token",
                required=True,
                location="header",
                schema=coreschema.String(),
                description="Authorization token",
            )
        ]
    )

    queryset = Company.objects.all().order_by('id')
    serializer_class = CompanySerializer
    authentication_classes = (authentication.CustomTokenAuthentication,)
    pagination_class = pagination.LinkHeaderPagination
```

```
class ProductGroupViewSet(BaseModelViewSet):
    """
    Manage product group
    ---
    list:
        parameters:
        - name: X-Auth-Token
          required: true
          paramType: header
        - name: page
          required: false
          paramType: query
          description: Page number
        - name: page_size
          required: false
          paramType: query
          description: Number of records to fetch. (Default is 30, max of 100)
    create:
        parameters:
        - name: X-Auth-Token
          required: true
          paramType: header
    destroy:
        parameters:
        - name: X-Auth-Token
          required: true
          paramType: header
    retrieve:
        parameters:
        - name: X-Auth-Token
          required: true
          paramType: header
    update:
        parameters:
        - name: X-Auth-Token
          required: true
          paramType: header
    """
    schema = AutoSchema(
        manual_fields=[
            coreapi.Field(
                name="X-Auth-Token",
                required=True,
                location="header",
                schema=coreschema.String(),
                description="Authorization token",
            )
        ]
    )

    queryset = ProductGroup.objects.all().order_by('id')
    serializer_class = ProductGroupSerializer
    authentication_classes = (authentication.CustomTokenAuthentication,)
    pagination_class = pagination.LinkHeaderPagination
```

From **api/dashboard/viewset/**

```
class AppLanguageViewSet(viewsets.ModelViewSet):
    queryset = AppLanguage.objects.all()
    serializer_class = AppLanguageSerializer

    schema = AutoSchema(
        manual_fields=[
            coreapi.Field(
                name="code",
                required=True,
                location="formData",
                schema=coreschema.String(),
                description="Language code",
            ),
            coreapi.Field(
                name="name",
                required=True,
                location="formData",
                schema=coreschema.String(),
                description="Language name",
            )
        ]
    )

    @exceptions_catched
    @staff_login_required
    def list(self, request):
        appLanguage = AppLanguageSerializer(
            AppLanguage.objects.all(),
            many=True,
        ).data
        return Response({'success': True, 'appLanguage': appLanguage})
```

### Adding schema for (function-based views) endpoint for swagger UI.

**IMPORTANT**

* Define the **AutoSchema** for the function-based view endpoint.
* Use **@schema** decorator to attach the use-defined-schema to the function-based view endpoint.
* **location** values {**query, header, path, formData, body, form**}
* Remove the yaml docstring part after the use of schema is working.
* [OpenAPI](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#parameterObject)
* [Django Rest Framework Schemas](http://www.django-rest-framework.org/api-guide/schemas/)

```
import coreapi
import coreschema

from rest_framework.decorators import (
    api_view,
    schema,
)
```

```
login_schema = AutoSchema(
    manual_fields=[
        coreapi.Field(
            name="username",
            required=True,
            location="formData",
            schema=coreschema.String(),
            description="username",
        ),
        coreapi.Field(
            name="password",
            required=True,
            location="formData",
            schema=coreschema.String(),
            description="password",
        ),
        coreapi.Field(
            name="language_code",
            required=True,
            location="formData",
            schema=coreschema.String(),
            description="Language code (EN, CN, TC, FR, JP, KR)",
        )
    ]
)


@api_view(['POST'])
@schema(login_schema)
@exceptions_catched
def login(request):
    """
    Authenticate user in login process.
    ---
    parameters:
    - name: username
      required: true
      paramType: form
    - name: password
      required: true
      paramType: form
    - name: language_code
      description: EN, CN, TC, FR, JP, KR
      required: true
      paramType: form
    """

    try:
        user = User.objects.get(username=request.data['username'])
        if not check_password(request.data['password'], user.password):
            return Response({
                'success': False,
                'err_code': ErrorCode.INVALID_PASSWORD,
                'error': 'Invalid username or password'
            }, status=status.HTTP_401_UNAUTHORIZED)

        # Fake login - just check if user credentials exist on this server and
        # nothing else.
        if 'fake' in request.query_params:
            return Response({'success': True})

        if user.is_login:
            """
            User is already logged in with other device so we'll delete tokens
            of that device.
            """
            push_notification.delete_devices(user.id)
        elif not user.active:
            return Response({
                'success': True,
                'err_code': ErrorCode.USER_NOT_ACTIVE
            }, status=status.HTTP_403_FORBIDDEN)

        user.language_id = AppLanguage.objects.get(
            pk=request.data['language_code']
        )
        user.token = uuid4().hex
        user.is_login = True
        user.login_count += 1
        user.save()

        serializer = UserLoginSerializer(user)

        return Response({'success': True, 'user': serializer.data})
    except User.DoesNotExist:
        return Response(
            {'success': False, 'error': 'Invalid username or password'},
            status=status.HTTP_401_UNAUTHORIZED
        )
    except AppLanguage.DoesNotExist, e:
        logging.getLogger('django').exception(e.message)
        return Response({'success': False, 'error': 'Invalid language'},
                        status=status.HTTP_400_BAD_REQUEST)
```

```
user_details_schema = AutoSchema(
    manual_fields=[
        coreapi.Field(
            name="X-Auth-Token",
            required=True,
            location="header",
            schema=coreschema.String(),
            description="Authorization token",
        )
    ]
)


@api_view(['GET'])
@schema(user_details_schema)
@exceptions_catched
@login_required
def user(request):
    """
    Get single use details.
    ---
    parameters:
    - name: X-Auth-Token
      required: true
      paramType: header
    """
    user = request.authenticated_user

    serializer = serializers.UserInfoSerializer(user)
    return Response({
        'success': True,
        'user': quiz_helper.with_total_points(
            serializer.data,
            user.id)
    })
```

```
check_version_schema = AutoSchema(
    manual_fields=[
        coreapi.Field(
            name="company_id",
            required=True,
            location="path",
            schema=coreschema.String(),
            description="Company ID",
        ),
        coreapi.Field(
            name="user_id",
            required=False,
            location="path",
            schema=coreschema.Integer(),
            description="User ID",
        )
    ]
)


@api_view(['GET'])
@schema(check_version_schema)
def check_version(request):
    """
    Returns iOS and Android app versions with download urls.
    ---
    parameters:
    - name: company_id
      required: false
      paramType: query
    - name: user_id
      required: false
      paramType: query
    """
    app = GetApp(request).get()

    if app:
        return Response(
            {
                'success': True,
                'version': VersionSerializer(app).data
            },
            status=200
        )

    return Response({'success': False, 'message': 'Not found.'}, status=404)
```
