# Difference between MVC and MVT Design Pattern

1. **Model View Controller (MVC)**

	* **Model** is the central component of this architecture and manages the data, logic as well as other constraints of the application.
	* **View** deals with how the data will be displayed to the user and provides various data representation components.
	* **Controller** manipulates the Model and renders the view by acting as a bridge between both of them.

1. **Model View Template (MVT)**

	* **Model** similar to MVC acts as an interface for your data and is basically the logical structure behind the entire web application which is **represented by a database such as MySql, PostgreSQL.**
	* **View** executes the **business logic and interacts with the Model and renders the template. It accepts HTTP request and then return HTTP responses.**
	* **Template** is the component which makes MVT different from MVC. Templates act as the **presentation layer and are basically the HTML code that renders the data.** The content in these files can be either static on dynamic.
	
# Status codes

## HTTP response code

Every HTTP response starts off with a single line that contains the HTTP response code. It is a three digit number that contains the server's idea of the status for the request. The numbers are detailed in the HTTP standard specifications but they are divided into ranges that basically work like this:

| Code   | Meaning                            | DRF          | DRF - Helper Functions |
| -------| ---------------------------------  | ------------ | ---------------------- |
| 1xx    | Transient code, a new one follows  | Information  | is_informational()     |
| 2xx    | Things are OK                      | Successful   | is_success()           |
| 3xx    | The content is somewhere else      | Redirection  | is_redirect()          |
| 4xx    | Failed because of a client problem | Client Error | is_client_error()      |
| 5xx    | Failed because of a server problem | Server Error | is_server_error()      |

# Raising Errors

1. `NotImplementedError`

		def get_comment_count(self, obj):
		    raise NotImplementedError(
		        'Required to implement get_memory_source_content method. Abstract implementation!'
		    )
		      