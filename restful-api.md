# What is REST?

An **application programming interface (API)** defines the kinds of **calls** or **requests** that can be made, how to make them, **the data formats that should be used**, the conventions to follow, etc. 

**REST** is acronym for **REpresentational State Transfer**. It's an **architectural pattern for creating web services.**

The key abstraction of information in REST is a **resource**. Any information that can be named can be a resource: a document or image, a temporal service, a **collection of other resources**, a non-virtual object (e.g. a person), and so on. 

REST uses a **resource identifier** to identify the particular resource involved in an interaction between components.

The state of the resource at any **particular timestamp** is known as **resource representation**.

| REST API Methods  | Http Request Methods  | 
| ------------------| --------------------- | 
| [C] reate         | POST                  | 
| [R] ead           | GET                   | 
| [U] pdate         | PUT/PATCH             | 
| [D] elete         | DELETE                | 


The term **endpoint** is focused on the **URL that is used to make a request**.

The term **resource** is focused on the **data set that is returned by a request**.

**Sample endpoints**

* `/api/v1/knowledges/`,
* `/api/v2/knowledges/`
* `/api/v2/media/`
* `/api/v2/media/resources/`


# Request Template

## Request Headers

* **Authorization Token:** `X-Auth-Token: *****9c38f5845e0a9d4fa37a1f*****`
* **OAuth2 Access Token:** `Authorization: Bearer *****4RpQKHV4eAKyRqxbhGec*****`

## Response Headers

* **Next Page Link:** `next-page-link: http://0.0.0.0:8000/api/v1/users/?page=3`
* **Previous Page Link:** `prev-page-link: http://0.0.0.0:8000/api/v1/users/`
* **Total Response Count:** `count: 943`

**IMPORTANT:** The current user is excluded from the listing and total count in users endpoint.
 
## GET method

* **pagination:** `page=1&page_size=10` `(page_size=30, max_page_size=100)`
* **ordering (comma separated):** `o=name,company_id`
* **searching:** `q=video`
* **more info (comma separated):** `include=count`
* **route (list):** GET /api/v1/users/
* **route (detail):** GET /api/v1/users/{id}/

## POST method

* **route:** POST /api/v1/users/

## PUT / PATCH method

* **route:** PUT /api/v1/users/{id}/
* **route:** PATCH /api/v1/users/{id}/

## DESTROY method

* **route:** DESTROY /api/v1/users/{id}/

# Status codes

Every HTTP response starts off with a single line that contains the **HTTP response code**. It is a three digit number that contains the server's idea of the status for the request. The numbers are detailed in the HTTP standard specifications but they are divided into ranges that basically work like this:

1. **1xx**	- Transient code, a new one follows. **DRF: Informational**
2. **2xx**	- Things are OK. **DRF: Successful**
3. **3xx**	- The content is somewhere else	Redirection. **DRF: Redirection**
4. **4xx**	- Failed because of a client problem. **DRF: Client Error**
5. **5xx**	- Failed because of a server problem. **DRF: Server Error**