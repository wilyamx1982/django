# Serializers

1. Getting the **request user**

		def validate_knowledge(self, obj):
		    try:
		        knowledge = Knowledge.objects.get(pk=obj.id)
		    except Knowledge.DoesNotExist:
		        raise serializers.ValidationError('Knowledge does not exist.')
			
		    request = self.context.get('request')
		    if request and hasattr(request, 'authenticated_user'):
		        user = request.authenticated_user
		        if UserKnowledgeResult.objects.filter(knowledge=obj, user=user).exists():
		            raise serializers.ValidationError('Knowledge result already exist.')
			
		    return knowledge
			
			def get_serializer_context(self):
		    context = super(KnowledgeCardViewSet, self).get_serializer_context()
		    if hasattr(self.request, 'authenticated_user'):
		        user = self.request.authenticated_user
		        context.update({
		            'user_id': user.id,
		            'user_group_id': user.user_group_id.id,
		            'language_code': self.request.query_params.get('language_code')
		        })
		    return context

1. `serializer.Meta.model` 
1. `serializer.Meta.model.__name__` 
1. Adding optional **include** params

		def to_representation(self, obj):
		    data = super(FeedSerializer, self).to_representation(obj)
		    include = self.context.get('include')
		    if include:
		        include_keys = include.split(',')
		        if 'more_details' in include_keys:
		            data['more_details'] = self.get_more_details(obj)

1. `user` field using `request.user` for model create

		class CompanyOwnerSerializer(serializers.ModelSerializer):
		    class Meta:
		        model = CompanyOwner
		        fields = [
		            'id',
		            'company',
		            'created_at'
		        ]
		
		    def create(self, validated_data):
		        request = self.context.get('request')
		        if request and hasattr(request, 'authenticated_user'):
		            user = request.authenticated_user
		            validated_data['owner'] = user
		
		        return super(CompanyOwnerSerializer, self).create(validated_data)

1. **write** only field

		class UserKnowledgeQuizAnswerSerializer(serializers.ModelSerializer):
			    # needed for validation of the answer
			    knowledge_quiz_id = serializers.IntegerField(write_only=True, required=True)
			
			    class Meta:
			        model = UserKnowledgeQuizAnswer
			        fields = [
			            'id',
			            'answer',
			            'created_at',
			            'knowledge_quiz_id'
			        ]
			
			    def create(self, validated_data):
			        knowledge_quiz_id = validated_data['knowledge_quiz_id']
			        if knowledge_quiz_id is not None:
			            del validated_data['knowledge_quiz_id']
			
			        return super(UserKnowledgeQuizAnswerSerializer, self).create(validated_data)

1. `liked = serializers.SerializerMethodField(method_name='has_liked’)` 
1. `company_name = serializers.CharField(source='company_id.name')` 
1. method for **write** and **read only**

		class ProductGroupSerializer(serializers.ModelSerializer):
			user_group_list = serializers.SerializerMethodField()
			user_group = serializers.ListField(write_only=True)
				
			class Meta:
			    model = ProductGroup
			    fields = [
			        'user_group',
			        'user_group_list',
			    ]
				
			def get_user_group_list(self, obj):
			    return ProductUserGroupSerializer(
			        obj.user_group.all(), many=True
			    ).data



