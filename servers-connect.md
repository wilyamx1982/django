## AWS Regions (Production API Gateway)

* **US West (N. Carolina)** - hosted in **albert.today** a.k.a. **Albert** apps like
* worker-stg.albert.today
* worker.albert.today

		(albert, Albert),
		(ugo, UGO),
		(euniversite, E-DAILY)

* **Asia Pacific (Singapore)** - hosted in **dailytrainingpartner.com** a.k.a. **DTP** apps like 
* worker.dailytrainingpartner.com 

		(miss, Miss Bee),
		(mister, Mister Bee)
		(en, Miss Bee / Mister Bee),
		(testing app, (Testing) App),
		(Bobby),
		(Jenny)

## Hosted apps from Staging / Development Server

	(u'ugo', u'UGO'),
	(u'albert', u'Albert'),
	(u'ugo', u'UGO (on Store)'),
	(u'albert', u'Albert (on store)'),
	(u'euniversite', u'E-DAILY'),
	(u'test', u'test'),
	(u'itraining', u'Internal Training'),
	(u'miss', u'Miss Bee'),
	(u'mister', u'Mister Bee'),
	(u'ugo2', u'UGO China (Not Used)'),
	(u'albert2', u'Albert China (Not Used)'),
	(u'smith', u'Smith')

## Working with Remote Server

	$ cd ~/.pems
	$ chmod 600 albert-stg.pem (1-time-setup only)
	$ chmod 600 albert-prod.pem (1-time-setup only)
	$ chmod 600 dtp-prod.pem (1-time-setup only)
	$ chmod 600 albert-test.pem (1-time-setup only)
	$
	$ ssh-keygen -f ~/.ssh/known_hosts -R ec2-api-stg.dailytrainingpartner.com (use when server signature was changed)

**Activating virtual environments**

**Note:** You need to locate the `.venv` folder to know all the virtual environment available

	ubuntu@ip-10-0-2-176:~$ ll
	total 52
	...
	drwxrwxr-x  3 ubuntu ubuntu 4096 Nov 20 01:59 .venv/
	-rw-rw-r--  1 ubuntu ubuntu 6672 Nov 20 01:59 Makefile
	drwxrwxr-x  6 ubuntu ubuntu 4096 Nov 20 01:55 ansible/
	drwxrwxr-x 20 ubuntu ubuntu 4096 Nov 20 02:05 app/	
	ubuntu@ip-10-0-2-176:~$ ll .venv
	total 12
	drwxrwxr-x 3 ubuntu ubuntu 4096 Nov 20 01:59 ./
	drwxr-xr-x 8 ubuntu ubuntu 4096 Nov 20 05:25 ../
	drwxrwxr-x 7 ubuntu ubuntu 4096 Nov 20 01:59 api/
	ubuntu@ip-10-0-2-176:~$ cd app
	ubuntu@ip-10-0-2-176:~/app$ source ~/.venv/api/bin/activate
	(api) ubuntu@ip-10-0-2-176:~/app$ 

### Working Directories from development server

```
$ ssh -i albert-test.pem ubuntu@api-server.public.ip (tittest)
```

```
ubuntu@ip-10-0-1-121:~$ ll ~/.venv
total 20
drwxrwxr-x  5 ubuntu ubuntu 4096 Feb  7 01:47 ./
drwxr-xr-x 10 ubuntu ubuntu 4096 Feb 14 08:07 ../
drwxrwxr-x  7 ubuntu ubuntu 4096 Feb  6 03:36 api/
drwxrwxr-x  7 ubuntu ubuntu 4096 Feb  7 01:48 titcms/
drwxrwxr-x  7 ubuntu ubuntu 4096 Feb  6 06:01 worker/

ubuntu@ip-10-0-1-121:~$ ls
api  cms  titcms  worker

ubuntu@ip-10-0-1-121:~$ cd api
ubuntu@ip-10-0-1-121:~/api$ source ~/.venv/api/bin/activate
(api) ubuntu@ip-10-0-1-121:~/api$ ./manage.py shell
Python 2.7.12 (default, Nov 12 2018, 14:36:49) 
[GCC 5.4.0 20160609] on linux2
Type "help", "copyright", "credits" or "license" for more information.
(InteractiveConsole)
>>> 
```

### Working Directories from api server

```
$ ssh -i albert-stg.pem ubuntu@api-server.public.ip (API Server - titstg)
$ ssh -i albert-prod.pem ubuntu@api-server.public.ip (API Server - titprod)
$ ssh -i dtp-prod.pem ubuntu@api-server.public.ip (API Server - dtpprod)
```

```
ubuntu@ip-10-0-1-61:~$ cd app
ubuntu@ip-10-0-1-61:~/app$ source ~/.venv/api/bin/activate
(api) ubuntu@ip-10-0-1-61:~/app$ ./manage.py shell
Python 2.7.12 (default, Nov 12 2018, 14:36:49) 
[GCC 5.4.0 20160609] on linux2
Type "help", "copyright", "credits" or "license" for more information.
(InteractiveConsole)
>>> 
```

### Working Directories from worker server

```
$ ssh -i albert-stg.pem ubuntu@worker-server.public.ip (Worker server - titstg)
$ ssh -i albert-prod.pem ubuntu@worker-server.public.ip (Worker server - titprod)
$ ssh -i dtp-prod.pem ubuntu@worker-server.public.ip (Worker server - dtpprod)
```

```
ubuntu@ip-10-0-1-186:~$ cd app
ubuntu@ip-10-0-1-186:~/app$ source ~/.venv/worker/bin/activate
(worker) ubuntu@ip-10-0-1-186:~/app$ ./manage.py shell
Python 2.7.12 (default, Nov 12 2018, 14:36:49) 
[GCC 5.4.0 20160609] on linux2
Type "help", "copyright", "credits" or "license" for more information.
(InteractiveConsole)
>>>
```

## Running Dashboard in local environment (cms-spa)

### 1. redis-server: running message queue

```
$ (tt-app-server) redis-server
...
32833:M 05 Sep 06:45:30.928 # Server initialized
32833:M 05 Sep 06:45:30.928 * DB loaded from disk: 0.000 seconds
32833:M 05 Sep 06:45:30.928 * Ready to accept connections
```

### 2. [django-rq](https://bitbucket.org/thinkittwice/django-rq/src/master/): running the workers

```
(tt-app-server) ./manage.py rqworker high default low
...
01:32:34 Registering birth of worker earvin-pc.84829
01:32:34 RQ worker u'rq:worker:earvin-pc.84829' started, version 0.11.0
01:32:34 *** Listening on high, default, low...
01:32:34 Sent heartbeat to prevent worker timeout. Next one should arrive within 420 seconds.
01:32:34 Cleaning registries for queue: high
01:32:34 Cleaning registries for queue: default
01:32:34 Cleaning registries for queue: low
01:32:34 *** Listening on high,default,low...
01:32:34 Sent heartbeat to prevent worker timeout. Next one should arrive within 420 seconds.
```

### 3. app-server

**.env (app-server)**

```
DATABASE_URL=mysql://root:password@localhost:3306/db_app_dev_poleluxe
DATABASE_URL_CMS=mysql://root:password@localhost:3306/db_dev_poleluxe
DATABASE_URL_ANALYTICS=mysql://root:password@localhost:3306/db_dev_analytics_v1_test?charset=utf8

DEVICE_USER_PASSWORD=device-user-password

CACHE_URL=redis://127.0.0.1:6379
```
**IMPORTANT: Creating auth-user and running the server**

```
(tt-app-server) ./manage.py setdeviceuser
(tt-app-server) $ make run
...
Starting development server at  http://0.0.0.0:9000/
```

The command will create **django.contri.auth.models.User** to app-server database

### 4. api-server

**.env (api)**

```
DATABASE_URL=mysql://root:password@localhost:3306/db_dev_poleluxe
DATABASE_URL_CMS=mysql://root:password@localhost:3306/db_app_dev_poleluxe
DATABASE_URL_ANALYTICS=mysql://root:password@localhost:3306/db_dev_analytics_v1_test?charset=utf8

APP_SERVER_AUTH_URL=http://127.0.0.1:9000/api/token-auth/
APP_SERVER_BASE_URI=http://127.0.0.1:9000
APP_SERVER_API_USER=deviceworker
APP_SERVER_API_PASS=device-user-password

CACHE_URL=redis://127.0.0.1:6379

# Baidu Push
BAIDUPUSH_DEVICE_TABLE=baidupush_devices
BAIDUPUSH_AWS_REGION=ap-southeast-1
SQS_QUEUE_URL=
BAIDUPUSH_APPLICATIONS={"albert":{"APP_ID":"1234","API_KEY":"QWERTY","SECRET_KEY":"ASDFG"}}
```

**Running the server**

```
(tt-api) $ make run
...
Starting development server at http://0.0.0.0:8000/
```

**Verify 1: PushNotificationDeviceHelper.login() --> print(self.auth)**

```
{
	'url': 'http://127.0.0.1:9000/api/token-auth/',
	'credentials': {
		'username': 'deviceworker',
		'password': 'device-user-password'
	}
}

```

**Verify 2: POST /api/v1/login using any PoleLuxe user credentials via Swagger**

```
{
  "user": {
    "id": 895,
    "username": "swagger",
    "name": "Jonathan Crane",
    "avatar_url": "https://tit-test.s3.amazonaws.com/WILLIAM/_GIT/ThinkItTwice/api/.test_dir/tmp6cTVTI.jpg",
    "company_id": 8,
    "company_name": "TIT Mister Bee",
    "department_name": "white",
    "position": "Corporate treasurer",
    "user_group_id": 86,
    "user_group_name": "Anna Brown",
    "stream_group": 3,
    "timezone": 8,
    "level": 0,
    "points": 0,
    "login_count": 54,
    "is_change_password": false,
    "receive_notification": true,
    "token": "ac76de28cdde43978b59a4bf1af3c451",
    "privileges": {
      "is_superuser": true,
      "is_staff": true
    },
    "active": true
  },
  "success": true
}

```

### 5. cms-server (if necessary)

**Running the server**

```
(tt-cms) $ make run
...
Starting development server at http://0.0.0.0:9001/
```

### 6. cms-spa-server

**cms-spa/src/env.json** host should be point to **api-server**

```
[
    {"host": "http://0.0.0.0:8000/"}
]
```

**Running the website**

```
(tt-cms-spa) $ nvm use --delete-prefix v6.14.4 (if necessary)
(tt-cms-spa) $ npm start
...
[16:34:38] Using gulpfile /WILLIAM/_GIT/ThinkItTwice/cms-spa/gulpfile.js
[16:34:38] Starting 'serve'...
[16:34:38] Finished 'serve' after 6.38 ms
[16:34:38] Server started at http://127.0.0.1:8080
```

Login using **PoleLuxe user** credentials from dashboard page.

### 7. django-albert (optional)

[Create swagger user](swagger.md)

```
(tt-django-albert) earvin-pc:django-albert wrena$ ./manage.py setswaggeruser password
```
